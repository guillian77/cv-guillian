<?php

/**
* WEBSITE SETTINGS
*/

class Conf {
	static $settings = array(
		/** ======
		* GENERAL
		*/
		'version' => "1.2.2",
		'debug' => 0,
		'cv' => "CV_Guillian_4.1.pdf",

		/** ======
		* NETWORKS
		*/
		'network' => array(
			'linkedin' => "https://www.linkedin.com/in/aufrere-guillian/",
			'git' => "https://bitbucket.org/guillian77/?visibility=public",
			'mail' => "guillian77270@gmail.com"
		),

		/** ======
		* REFERENCEMENT
		*/
		'keywords' => array(
			'url' => 'guillian-aufrere.fr',
			'title' => 'Développeur web et web mobile',
			'description' => "Curriculum Vitae de Guillian Aufrère, développeur web en formation",
			'keywords' => "nuxt, vuejs, HTML, CSS, PHP, bootstrap, laravel, SASS, compass, composer, linux, serveur"
		),
	);
}